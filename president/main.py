import random

'''
clubs - 0
hearts - 1
spades - 2
diamonds -3

jack - 11
queen - 12
king - 13
ace - 14

'''

class Card:

	suitList = ['clubs', 'hearts', 'spades', 'diamonds']
	rankList = [
				'null', 'null', '2', '3', '4', '5', '6',
				'7', '8', '9', '10', 'Jack', 'Queen', 
				'King', 'Ace']
	
	def __init__(self, suit, rank):
		self.suit = suit
		self.rank = rank
		
	
	def __str__(self):
		return (self.rankList[self.rank] + " of " + 
				self.suitList[self.suit])
		
	def __cmp__(self, other):
	
		#suits irrelevant in president
		
		if self.rank > other.rank: 
			return 1
			
		if self.rank < other.rank:
			return -1
			
		return 0
		
	
class Deck:
	def __init__(self):
		#populating the deck
		self.cards = []		
		for suit in range(4):
			for rank in range(2,15):		
				self.cards.append(Card(suit, rank))
	
	def __str__(self):
		s = ""
		for i in range(len(self.cards)):
			s = s + " "*i + str(self.cards[i]) + "\n"
		return s
		
	def shuffle(self):
		nCards = len(self.cards)
		for i in range(nCards):
			j = random.randrange(i, nCards)
			self.cards[i], self.cards[j] = self.cards[j], self.cards[i]
			
	def removeCard(self, card):
		if card in self.cards:
			self.cards.remove(card)
			return True
		else:
			return False
			
	def popCard(self):
		return self.cards.pop()
		
	def isEmpty(self):
		return (len(self.cards) == 0)
		
	def deal(self, hands, nCards=999):
		nHands = len(hands)
		for i in range(nCards):
			if self.isEmpty():
				break
			card = self.popCard()
			hand = hands[i % nHands]
			hand.addCard(card)
		
		
class Hand(Deck):
	def __init__(self, name=""):
		self.cards = []
		self.name = name
		self.next = None
		self.first = False
		
	def addCard(self, card):
		self.cards.append(card)
		
	def removeCard(self, card):
		if card in self.cards:
			self.cards.remove(card)
			print "%s has been played" %card
			return True
		else:
			return False 
	
	def __str__(self):
		s = "Hand " + self.name
		if self.isEmpty():
			return s + " is empty\n"
		else:
			return s + " contains\n" + Deck.__str__(self)
			

			

class CardGame:
	def __init__(self):
		self.deck = Deck()
		self.deck.shuffle()
		self.deck.shuffle()
		self.deck.shuffle()
		#maybe shuffle deck random amount of times
		
		
class FirstRoundHand(Hand):
	def sortcards(self):
	self.cards = sorted(self.cards)

	
class PresidentCardGame(CardGame):
	def play(self, names):
		self.hands = []
		for name in names:
			self.hands.append(FirstRoundHand(name)
			
		self.deck.deal(self.hands)
		print " The cards have been dealt"
		print self.hands
		
		for hand in self.hands:
			sortcards(hand)
			
		for index, hand in enumerate(self.hands):
			try:
				hand.next = self.hands[index + 1]
			
			except:
				break
			
		
		
		
		
	def playoneturn(self, i):
		table = []
		self.leader = None
		
		def playcard(hand, card):
			hand.removecard(card)
			table.append(card)
			print "%s was played by %s" %(card, hand)
			self.leader = hand
			turnhand = hand.next
			return turnhand
			
		if self.hands[i].isEmpty():
			return 0 
		
		for hand in self.hands:
			if (0,3) in hand.cards:
				playcard(hand, (0,3))
				hand.first = True
				break
				
		while len(table) <= len(self.hands):
			if turnhand.first == True:
				break
		
			if turnhand.cards[-1] < table[-1]:
				print "%s has passed their turn" %turnhand.name
				turnhand = turnhand.next
				continue
				
			for card in turnhand.cards:
				if card > table[-1]:
					playcard(turnhand, card)
					break
		
# need to make variable to keep track of winner because
#if people skip til we get back to the 1st player, the 2nd could be on top
#but current turn would be player1
		
		
			
		
		
	
class President:
	pass
	
class VicePresident:
	pass
	
class Citizen:
	pass
	
class Hoe:
	pass
	
class Hobo:
	pass