try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup
	
config = {
	'description': 'President Card Game',
	'author': 'Dmitri Kyle Brereton',
	'url': 'URL to get it at.',
	'download_url': 'Where to download it.',
	'author_email': 'mitrikyle@yahoo.com',
	'version': '0.1',
	'install_requires': ['nose'],
	'packages': ['NAME'],
	'scripts': [],
	'name': 'president'
}
setup(**config)